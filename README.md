# The Cat Collective's Leetcode Solutions

## Project Description
This repository contains the sources for The Cat Collective's solutions to leetcode problems. This repository will be updated from time to time with new solutions as we continue to work through the problems.

## Getting Started
Download the dependencies for the project and then clone the repo.

This project requires:
`git`

This project also utilizes `pre-commit`for static typechecking but does not directly depend upon it. To install `pre-commit`, please ensure you have `python3.6` or newer installed and then run the following command:
```bash
$ pip install pre-commit
$ pre-commit install
```
To verify for yourself that this code uses the proper type annotations, run the following command after installing mypy and cloning the repo as (described below):
```bash
$ pre-commit run --all-files
```

## Cloning the repo
Run the following command to clone the sources:
```bash
$ git clone https://gitlab.com/the-cat-collective/tcc-code/leetcode.git
```
## Project Structure
The root of the project contains three folders, "easy", "medium" and "hard" corresponding to the leetcode assigned difficulty of the problem. All leetcode problems of a specific difficulty reside in their respective folder. The project root also contains a README and LICENSE.

## Testing the Solutions
Unit tests will be available to the code along with the solutions for easy testing.

## Licensing
* This project is licensed under the Creative Commons Zero (CC0) licnese. Please refer to the [LICENSE](LICENSE) file for the full license text under which this repository is licensed.

* Two files from this repo are **NOT** licensed under CC0, and are instead licensed under the BSD 3-Clause license. For licensing information related to these components, please check the [LICENSING](LICENSING) file.

* For licensing information related to third-party components used in this project, please check the [LICENSING](LICENSING.md) file.

## Disclaimer
None.
