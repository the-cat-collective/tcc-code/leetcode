#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# __init__.py file for the easy leetcode solutions folder
#
# SPDX-License-Identifier: SPDX-License-Identifier: CC0-1.0
# This work is dedicated to the public domain under the CC0 license.
#
# Author: The Cat Collective
