#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# The Cat Collective's Solution to Leetcode 35
#
# SPDX-License-Identifier: SPDX-License-Identifier: CC0-1.0
# This work is dedicated to the public domain under the CC0 license.
#
# Author: The Cat Collective

from typing import List
class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        for i in range(len(nums)):
            if nums[i] == target or nums[i] > target:
                return i
            i += 1
        return i
