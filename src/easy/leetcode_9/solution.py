#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# The Cat Collective's Solution to Leetcode 9
#
# SPDX-License-Identifier: SPDX-License-Identifier: CC0-1.0
# This work is dedicated to the public domain under the CC0 license.
#
# Author: The Cat Collective

class Solution:
    def isPalindrome(self, x: int) -> bool:
        return str(x)[::-1] == str(x)
