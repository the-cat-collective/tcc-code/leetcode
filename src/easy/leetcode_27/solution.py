#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# The Cat Collective's Solution to Leetcode 27
#
# SPDX-License-Identifier: SPDX-License-Identifier: CC0-1.0
# This work is dedicated to the public domain under the CC0 license.
#
# Author: The Cat Collective

from typing import List
class Solution:
    def removeElement(self, nums: List[int], val: int) -> List[int]:
        while val in nums:
            nums.remove(val)
        return nums
