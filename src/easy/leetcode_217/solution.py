#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# The Cat Collective's Solution to Leetcode 217
#
# SPDX-License-Identifier: SPDX-License-Identifier: CC0-1.0
# This work is dedicated to the public domain under the CC0 license.
#
# Author: The Cat Collective

from typing import List
class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        flag: bool = False
        nums.sort()
        if len(nums) <= 1:
            return flag
        for i in range(0, len(nums)):
            if nums[i] == nums[i+1]:
                return True
        return flag
