#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# The Cat Collective's Solution to Leetcode 1732
#
# SPDX-License-Identifier: SPDX-License-Identifier: CC0-1.0
# This work is dedicated to the public domain under the CC0 license.
#
# Author: The Cat Collective

from typing import List
class Solution:
    def largestAltitude(self, gain: List[int]) -> int:
        max_altitude: int = 0
        current_altitude: int = 0
        for i in gain:
            current_altitude += i
            max_altitude = max(max_altitude, current_altitude)
        return max_altitude
