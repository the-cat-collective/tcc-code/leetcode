## Project License
While a majority of this project is licensed under Creative Commons Zero (CC0), there are some components licensed under other licenses. Please refer to the [LICENSE](LICENSE) file for the full text of the project's license. 3rd party code is generally under totally seperate licenses too, all of which is shown below.

## Third-Party Components
1. mypy
   - Authors: mypy Development Team
   - Project GitHub: [python/mypy](https://github.com/python/mypy)
   - License: MIT License
   - Original Project's License: [LICENSE](https://github.com/python/mypy/blob/master/LICENSE)
   - Credits: [CREDITS](https://github.com/python/mypy/blob/master/CREDITS)
